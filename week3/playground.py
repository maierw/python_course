# name = input("Please enter the given name of the student")
# lastname = input("Please enter the surname of the student")
# field = input("Please enter the field of study of the student")

# print((name,lastname,field)


# emojis = {
#     "happy": "😃",
#     "heart": "😍",
#     "rotfl": "🤣",
#     "smile": "😊",
#     "crying": "😭",
#     "kiss": "😘",
#     "clap": "👏",
#     "grin": "😁",
#     "fire": "🔥",
#     "broken": "💔",
#     "think": "🤔",
#     "excited": "🤩",
#     "boring": "🙄",
#     "winking": "😉",
#     "ok": "👌",
#     "hug": "🤗",
#     "cool": "😎",
#     "angry": "😠",
#     "python": "🐍"
# }

# sentence = input("Please enter a sentence:")

# words = sentence.split()
# new_sentence = ""
# for word in words:
#     if emojis.get(word):
#         new_sentence = new_sentence + " " + emojis[word]
#     else:
#         new_sentence = new_sentence + " " + word

# print(new_sentence)

magicNumber = input("Please enter the number of places to shift:")
if not magicNumber.isdecimal():
    print("You need to enter a number between 0 and 25!")
else:
    magicNumber = int(magicNumber)
    if 0 < magicNumber < 26 or magicNumber == 0:
        sentence = input("Please enter a sentence:")
        sentence = sentence.lower()

        letters = "abcdefghijklmnopqrstuvwxyz"
        result = ""

        for letter in sentence:
            if not letter in letters:
                result += letter
            else:
                indexOfLetter = letters.index(letter)
                newIndex = indexOfLetter + magicNumber
                if newIndex + magicNumber > 25:
                    newIndex -= 26
                result += letters[newIndex]

        print("The encrypted sentence is:", result)
    else:
        print("You need to enter a number between 0 and 25!")
